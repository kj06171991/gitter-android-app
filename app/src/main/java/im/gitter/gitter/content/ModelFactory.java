package im.gitter.gitter.content;


import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONException;
import org.json.JSONObject;

import im.gitter.gitter.models.Group;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.models.User;
import im.gitter.gitter.utils.JSONFix;

/**
 * These methods dont belong as constructors as they dont use the whole json object.
 * For example, one to one room json also includes the user json, but these methods will not
 * parse the user out. They are designed to be used while parsing the whole json payload before
 * putting all the different objects in the database.
 *
 * Using these methods and *not* putting the results in the database means you are wasting
 * precious network time.
 */
public class ModelFactory {

    private final DateTimeFormatter dateParser = ISODateTimeFormat.dateTimeParser();

    public Room createRoom(JSONObject json) throws JSONException {
        return new Room(
                json.getString("id"),
                json.getString("name"),
                json.getString("url").substring(1),
                json.getString("url"),
                json.getString("avatarUrl"),
                JSONFix.optString(json, "topic"),
                json.optBoolean("oneToOne"),
                json.optBoolean("activity"),
                json.optBoolean("lurk"),
                json.getInt("unreadItems"),
                json.getInt("mentions"),
                json.getBoolean("roomMember"),
                json.isNull("lastAccessTime") ? null : dateParser.parseDateTime(json.getString("lastAccessTime")).getMillis(),
                json.isNull("user") ? null : json.getJSONObject("user").getString("id"),
                JSONFix.optString(json, "groupId")
        );
    }

    public User createUser(JSONObject json) throws JSONException {
        return new User(
                json.getString("id"),
                json.getString("username"),
                json.getString("displayName"),
                json.getString("avatarUrl")
        );
    }

    public Group createGroup(JSONObject json) throws JSONException {
        return new Group(
                json.getString("id"),
                json.getString("uri"),
                json.getString("name"),
                json.getString("avatarUrl"),
                JSONFix.optString(json.getJSONObject("backedBy"), "type"),
                JSONFix.optString(json.getJSONObject("backedBy"), "LinkPath")
        );
    }
}
